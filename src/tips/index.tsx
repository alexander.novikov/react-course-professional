// Core
import React from 'react'
import { render } from 'react-dom';
import { HomePage } from './pages'


import './theme/main.scss'

render(<HomePage />, document.getElementById('root'));
