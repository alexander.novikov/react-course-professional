import HomePage from './home';
import PublishPage from './publish';
import SearchPage from './search';
import TagsPage from './tags';

export { HomePage, PublishPage, SearchPage, TagsPage};