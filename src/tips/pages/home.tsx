// Core
import React from 'react'
import { Nav } from '../components'

import { icons } from '../theme/icons/tag'

export default () => {
    return <section className="layout">
        <section className="hero">
            <div className="title">
                <h1>Типсы и Триксы</h1>
                <h2>React</h2>
            </div>

            <div className="tags">
                <span>
                    <icons.React /> React
                </span>
            </div>

        </section>
        <Nav/>
    </section>
};