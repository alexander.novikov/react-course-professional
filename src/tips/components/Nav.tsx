// Core
import React from 'react'

import { icons } from '../theme/icons/nav'

export default () => {
    return <nav className="nav">
        <h1>Т и Т</h1>
        <a>
            <icons.Home /> Все темы
        </a>
        <a>
            <icons.Tag /> По тегам
        </a>
        <a>
            <icons.Publish /> Опубликовать
        </a>
        <a>
            <icons.Search /> Поиск
        </a>
    </nav>
};
